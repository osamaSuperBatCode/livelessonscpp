#include <iostream>
#include <conio.h>
#include "maximum.h"

using std::cout;
using std::cin;
using std::endl;

int main()
{
	
	int int1, int2, int3;
	cout << "Input three integer values: ";
	cin >> int1 >> int2 >> int3;
	cout << "The Maximum of the integer value is: "
		<< maximum(int1, int2, int3) << endl;

	double double1, double2, double3;
	cout << "Input three double values: ";
	cin >> double1 >> double2 >> double3;
	cout << "The Maximum of the double value is: "
		<< maximum(double1, double2, double3) << endl;

	char char1, char2, char3;
	cout << "Input three character values: ";
	cin >> char1 >> char2 >> char3;
	cout << "The Maximum of the character  value is: "
		<< maximum(char1, char2, char3) << endl;

	_getch();
	return 0;


}