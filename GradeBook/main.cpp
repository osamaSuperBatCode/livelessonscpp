#include <iostream>
#include <conio.h>
#include "GradeBook.h";


using std::cout;
using std::endl;

int main()
{
	
	GradeBook myGradeBook("CS101 C++");

	myGradeBook.displayMessage();
	//myGradeBook.determineClassAverage();
	myGradeBook.inputGrades();
	myGradeBook.displayGradeReport();
	cout << endl;
	_getch();
	return 0;
}