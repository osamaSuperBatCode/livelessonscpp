#include <iostream>
#include <conio.h>

using std::cout;
using std::endl;

void staticArrayInit(void);
void automaticArrayInit(void);

int main()
{
	cout << "First Call to each Function:\n";
	staticArrayInit();
	automaticArrayInit();

	cout << "\n\nSecond Call to each function: \n";
	staticArrayInit();
	automaticArrayInit();
	cout << endl;
	_getch();
	return 0;
}

void staticArrayInit(void)
{
	static int array1[3]; //REMEMBER!!! STATIC ARRAYS DONT NEED INITIALIZATION!!!

	cout << "\nValues on entering static int array:\n";
	for (int i = 0; i < 3; i++)
	{
		cout << "array1[" << i << "] = " << array1[i] << " ";
	}

	cout << "\nValues on exiting the StaticIntArray:\n";

	for (int i = 0; i < 3; i++)
	{
		cout << "array1[" << i << "] = " << (array1[i] += 5) << " ";
	}
}

void automaticArrayInit(void)
{
	int array2[3] = { 1, 2, 3 }; 

	cout << "\nValues on entering automatic int array:\n";
	for (int i = 0; i < 3; i++)
	{
		cout << "array2[" << i << "] = " << array2[i] << " ";
	}

	cout << "\nValues on exiting the automaticIntArray:\n";

	for (int i = 0; i < 3; i++)
	{
		cout << "array2[" << i << "] = " << (array2[i] += 5) << " ";
	}
}