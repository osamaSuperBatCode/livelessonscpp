#include <iostream>
#include <conio.h>

using std::cout;
using std::endl;

int main()
{
	int c;
	c = 5;

	cout << c << endl;
	cout << c++ << endl;
	cout << c << endl << endl;

	c = 5;
	cout << c << endl;
	cout << ++c << endl;
	cout << c << endl << endl;

	_getch();
	return 0;

}