#include <iostream>
#include <conio.h>

using std::cout;
using std::endl;

#include <iomanip>
using std::setw;

int main()
{
	int n[10]; //the array
	for (int i = 0; i < 10; i++)
	{
		n[i] = 0; //initialize all elements in the array to 0
	}

	cout << "Element" << setw(13) << "Value" << endl;

	for (int j = 0; j < 10; j++)
	{
		cout << setw(7) << j << setw(13) << n[j] << endl;
	}

	_getch();
	return 0;
}