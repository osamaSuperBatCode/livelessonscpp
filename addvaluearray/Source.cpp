#include <iostream>
#include <conio.h>

using std::cout;
using std::endl;

#include <iomanip>

using std::setw;

int main()
{
	int n[10] = { 32, 05, 87, 96, 45 };//, 12, 36, 25, 01, 3 }; // int n[] ={Number of initializers}; 
	//if specified size of array is not completely filled, then rest of the elements of the array are initialized to 0

	cout << "Element" << setw(13) << "Value" << endl;

	for (int i = 0; i < 10; i++)
	{
		cout << setw(7) << i << setw(13) << n[i] << endl;
	}

	_getch();
	return 0;
}