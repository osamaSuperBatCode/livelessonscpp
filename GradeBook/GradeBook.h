#include <string>

using std::string;

class GradeBook
{
public:
	GradeBook(string name);
	void setCourseName(string name);
	string getCourseName();
	void displayMessage();
	void determineClassAverage();
	void inputGrades();
	void displayGradeReport();

private:
	string courseName;
	int aCount;
	int bCount;
	int cCount;
	int dCount;
	int fCount;
};
