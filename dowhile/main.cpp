#include <iostream>
#include <conio.h>

using std::cout;
using std::endl;

int main()
{
	int counter = 1;

	do 
	{
		cout << counter << " ";
		counter++;
	} while (counter <= 10);

	cout << endl;
	_getch();
	return 0;
}