#include <iostream>
#include <conio.h>

using std::cout;
using std::endl;

int main()
{
	int count;

	for (count = 1; count <= 10; count++)
	{
		if (count == 5)
		{
			break;
			//continue;
		}

		cout << count << endl;
	}

	cout << "Broke out of the loop at count = " << count << endl;	
	_getch();
	return 0;
}