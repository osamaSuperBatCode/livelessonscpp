#include <iostream>
#include <conio.h>
using std::cout;
using std::cin;
using std::endl;

inline double cube(const double side)
{
	return side * side * side;
}

int main()
{
	double sideValue;
	cout << "Enter the side length of your cube" << endl;
	cin >> sideValue;

	cout << "Volume of cube with side value " << sideValue << " is " << cube(sideValue) << endl;
	_getch();
	return 0;
}