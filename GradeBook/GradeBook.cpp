#include <iostream>
#include <iomanip>
#include "GradeBook.h"

using std::cout;
using std::cin;
using std::endl;
using std::fixed;

using std::setprecision;

GradeBook::GradeBook(string name)
{
	setCourseName(name);
	aCount = 0;
	bCount = 0;
	cCount = 0;
	dCount = 0;
	fCount = 0;
}

void GradeBook::setCourseName(string name)
{
	if (name.length() <= 25)
	{
		courseName = name;
	}

	if (name.length() >= 25)
	{
		courseName = name.substr(0, 25);
		cout << "Course name exceeds max limit of 25, accepting only first 25 chars\n";
	}
}

string GradeBook::getCourseName()
{
	return courseName;
}

void GradeBook::displayMessage()
{
	cout << "Welcome to grade book for\n" << getCourseName() << "!" << endl;
}

void GradeBook::determineClassAverage()
{
	int total;
	int gradeCounter;
	int grade;
	double average;

	total = 0;
	gradeCounter = 0;

	cout << "Enter Grade or -1 to quit ";
	cin >> grade;

	while (grade != -1)
	{
		
		total = total + grade;
		gradeCounter = gradeCounter + 1;

		cout << "Enter Grade or -1 to quit ";
		cin >> grade;
	}

	if (gradeCounter != 0)
	{
		average = static_cast<double>(total) / gradeCounter;
		cout << "\nAverage grade is: " << setprecision(2) << fixed << average << endl;
		cout << "\nTotal of " << gradeCounter << " grades is: " << total;
	}
	else
		cout << "No Grades Entered";
}

void GradeBook::inputGrades()
{
	int grade;

	cout << "Enter the letter grades." << endl << "Enter the EOF char to end the input" << endl;

	while ((grade = cin.get()) != EOF)
	{
		switch (grade)
		{
		case 'A':
		case 'a':
			aCount++;
			break;

		case 'B':
		case 'b':
			aCount++;
			break;

		case 'C':
		case 'c':
			cCount++;
			break;

		case 'D':
		case 'd':
			dCount++;
			break;

		case 'F':
		case 'f':
			fCount++;
			break;

		case '\n':
		case '\t':
		case ' ':
			break;

		default:
			cout << "No such grade" << endl << "Enter a new grade" << endl;
			break;
		}
	}
}

void GradeBook::displayGradeReport()
{
	cout << "\n\nNumber of Students who received each letter grade"
		<< "\nA: " << aCount
		<< "\nB: " << bCount
		<< "\nC: " << cCount
		<< "\nD: " << dCount
		<< "\nF: " << fCount << endl;
}