#include <iostream>
#include <conio.h>
#include <iomanip>

using std::setw;
using std::cout;
using std::endl;

void modifyArray(int array1[], int sizeOfArray); //whenever array is passed in function, it is passed by reference
void modifyElement(int arrayElement); // whenever an indie element from array is passed, its copy is passed.

int main()
{
	const int arraySize = 5;
	int a[arraySize] = { 1, 2, 3, 4, 5, }; //REMEMBER!!! Only a const or an explicit value can be passed as a size of array.

	cout << "\nEffects of passing an entire array by reference";
	cout << "\n\nThe Values of the original Array are: \n";

	for (int i = 0; i < arraySize; i++)
	{
		cout << setw(3) << a[i];
	}

	cout << endl;

	modifyArray(a, arraySize);
	cout << "The values of the modified Array are";

	for (int j = 0; j < arraySize; j++)
	{
		cout << setw(3) << a[j];
	}

	cout << "\n\n\nEffects of passing array elements by value are: "
		<< "\n\na[3] before modifyElement: " << a[3] << endl;

	modifyElement(a[3]);
	cout << "a[3] after modifyElement is " << a[3] << endl;

	_getch();
	return 0;
}

void modifyArray(int array1[], int sizeOfArray)
{
	for (int i = 0; i < sizeOfArray; i++)
	{
		array1[i] *= 2;
	}
}

void modifyElement(int arrayElement)
{
	cout << "Value of element in modify element is: " << (arrayElement *= 2) << endl;
}