#include <iostream>
#include <conio.h>

using std::cout;
using std::endl;
using std::boolalpha;

int main()
{
	cout << boolalpha << "Logical AND (&&)"
		<< "\nfalse && false: " << (false && false)
		<< "\nfalse && true: " << (false && true)
		<< "\ntrue && false: " << (true && false)
		<< "\ntrue && true: " << (true && true) << "\n\n";

	cout << "Logical OR (||)"
		<< "\nfalse || false: " << (false || false)
		<< "\nfalse || true: " << (false || true)
		<< "\ntrue || false: " << (true || false)
		<< "\ntrue || true: " << (true || true) << "\n\n";

	cout << "Logical NOT (!)"
		<< "\n!false: " << (!false)
		<< "\n!true: " << (!true);

	_getch();
	return 0;
}