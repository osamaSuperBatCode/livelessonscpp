#include <iostream>
#include <conio.h>

using std::cout;
using std::cin;
using std::endl;

int main()
{
	//REMEMBER IN C++ ARRAYS DONT HAVE BOUNDS CHECKING!!!! if u give a value more than the specified bound, then it may override other variables stored in memory (This is called a buffer overrun)
	char string1[20]; //Reserves 20 Spaces
	char string2[] = "String Literal"; //Reserves 15 spaces, counting the space, and in the end, \0, which is the null character

	cout << "Enter the string \"Hello There \": ";
	cin >> string1; //Reads only "hello", the remainder is terminated. Remainder is waiting to be read.

	cout << "string1 is: " << string1 << "\nstring2 is: " << string2;

	cout << "\nstring1 with spaces between characters is : \n";

	// \0 is the null character placed in a char array at the end
	for (int i = 0; string1[i] != '\0'; i++)
	{
		cout << string1[i] << ' ';
	}

	cin >> string1; //Now reads the word "there", the space character is thrown away, however cin.get() reads all indie chars.
	cout << "\nstring1 is: " << string1 << endl;

	_getch();
	return 0;
}

