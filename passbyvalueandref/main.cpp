#include <iostream>
#include <conio.h>
using std::cout;
using std::endl;

int squareByValue(int value);
int squareByReference(int &refValue);

int main()
{
	int x = 2;
	int z = 4;

	cout << "x = " << x << " before squaredByValue\n";
	cout << "Value returned by squareByValue: "
		<< squareByValue(x) << endl;
	cout << "x = " << x << " after squareByValue\n" << endl;

	cout << "z = " << z << " before squaredByReference\n";
	squareByReference(z);
	cout << "z = " << z << " after squaredByReference\n";

	_getch();
	return 0;
}

int squareByValue(int value)
{
	return value *= value;
}

int squareByReference(int &refValue)
{
	return refValue *= refValue;
}