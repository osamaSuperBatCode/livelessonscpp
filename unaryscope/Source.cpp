#include <iostream>
#include <conio.h>

using std::cout;
using std::endl;

int number = 7;
int main()
{
	double number = 10.5;

	cout << "Local Double value of number = " << number
		<< "\nGlobal int value of number  = " << ::number << endl;

	_getch();
	return 0;
}