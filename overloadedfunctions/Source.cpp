#include <iostream>
#include <conio.h>

using std::cout;
using std::endl;

int square(int x)
{
	cout << "Square of integer " << x << " is ";
	return x * x;
}

double square(double x)
{
	cout << "Square of Double " << x << " is ";
	return x * x;
}

int main()
{
	cout << square(7);
	cout << endl;
	cout << square(7.5);
	cout << endl;

	_getch();
	return 0;
}