#include <iostream>
#include <iomanip>
#include <cmath>
#include <conio.h>

using std::cout;
using std::endl;
using std::fixed;
using std::setw;
using std::setprecision;
using std::pow;

int main()
{
	double amount;
	double principal = 1000.0;
	double rate = .05;

	cout << "Year" << setw(21) << "Amount" << endl;
	cout << fixed << setprecision(2);

	for (int year = 1; year <= 10; year++)
	{
		amount = principal * pow(1.0 + rate, year);
		cout << setw(4) << year << setw(21) << amount << endl;
	}
	
	_getch();
	return 0;
}