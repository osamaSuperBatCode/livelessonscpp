#include <iostream>
#include <conio.h>
using std::cout;
using std::endl;

void useLocal();
void useStaticLocal();
void useGlobal();

int x = 1; //global variable

int main()
{
	cout << "Global x in main is" << x << endl;

	int x = 5; //local variable to main

	cout << "Local x in main's outer scope is " << x << endl;
	{//start new scope
		int x = 7;
		cout << "Local x in main's inner scope is " << x << endl;
	} //end of new scope
	cout << "Local x in main's outer scope is " << x << endl;

	useLocal();
	useStaticLocal();
	useGlobal();
	useLocal();
	useStaticLocal();
	useGlobal();

	cout << "\nLocal x in main is " << x << endl;

	_getch();
	return 0;
}

void useLocal()
{
	int x = 25;

	cout << "\nLocal x is " << x << " on entering useLocal" << endl;
	x++;
	cout << "Local x is " << x << " on exiting useLocal" << endl;
}

void useStaticLocal() /* IMPORTANT!!!!!  */
{
	static int x = 50; /* STATIC VARIABLES ARE LOCALLY SCOPED BUT LAST THROUGHT THE PROGRAM */

	cout << "\nLocal x is " << x << " on entering useStaticLocal" << endl;
	x++;
	cout << "Local x is " << x << " on exiting useStaticLocal" << endl;
}

void useGlobal()
{
	cout << "\nGlobal x is " << x << " on entering useGlobal" << endl;
	x *= 10;
	cout << "Global x is " << x << " on exiting useGlobal" << endl;
}