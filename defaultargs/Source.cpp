#include <iostream>
#include <conio.h>

using std::cout;
using std::endl;

int boxVolume(int length = 1, int width = 1, int height = 1); //remember, any argument with default, all the arguments to the right
															 // MUST HAVE a default argument, else you will get ERROR!!!!

int main()
{
	cout << "The default box volume is: " << boxVolume() << endl;

	cout << "With length is: " << boxVolume(10) << endl;

	cout << "With length and width is: " << boxVolume(10, 5) << endl;

	cout << "With length width and height is: " << boxVolume(10, 5, 15) << endl;

	_getch();
	return 0;
}

int boxVolume(int length, int width, int height)
{
	return length * width * height;
}